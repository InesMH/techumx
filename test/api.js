var mocha = require('mocha')
var chai = require('chai')
var chaiHttp=require('chai-http')
var server=require('../server')
var should = chai.should()

chai.use(chaiHttp)  // Configurar Chai con el módulo https
describe('Tests de conectividad',() => {
  it('Google funciona', (done)=>{
    chai.request('http://www.google.com.mx').get('/')
    .end((err,res) => {
      //console.log(res)
      res.should.have.status(200)
      done()
    })

  })

})

describe('Tests de API usuarios',()=>{
 it('Raìz de la API funciona', (done)=>{
   chai.request('http://localhost:3000').get('/v3')
   .end((err,res)=>{
     //console.log(res)
     res.should.have.status(200)
     res.body.should.be.a('array')
     done()
   })
 })
 it('Raìz de la API devuelve 2 colecciones', (done)=>{
   chai.request('http://localhost:3000').get('/v3')
   .end((err,res)=>{
     //console.log(res)
     console.log(res.body)
     res.should.have.status(200)
     res.body.should.be.a('array')
     res.body.length.should.be.eql(3)
     done()
   })
 })
 it('Raìz de la API devuelve los objetos correctos', (done)=>{
   chai.request('http://localhost:3000').get('/v3')
   .end((err,res)=>{
     //console.log(res)
     //(console.log(res.body)
     res.should.have.status(200)
     res.body.should.be.a('array')
     res.body.length.should.be.eql(3)
     for (var i = 0; i < res.body.length; i++) {
       res.body[i].should.have.property('recurso')
       res.body[i].should.have.property('url')
     }
     done()
   })
 })
})


describe('Tests de API movimientos',()=>{
 it('Raíz de la API movimientos contesta', (done)=>{
   chai.request('http://localhost:3000').get('/v3/movimientos')
   .end((err,res)=>{
     //console.log(res)
     res.should.have.status(200)
     res.body.should.be.a('array')
     done()
   })
 })
})

describe('Test de API movimientos',() => {
  it('Raiz de la api movimientos por id contesta',(done) =>{
    chai.request('http://localhost:3000')

    .get('/v3/movimientos/"GB68 ONWL 5540 5290 0198 46"')
    .end((err,res)=>{
      //console.log(res)
      res.should.have.status(200)
      res.body.should.be.a('array')
      res.body[0].should.have.property('idcuenta').which.is.eql("GB68 ONWL 5540 5290 0198 46")
      done()
    })
  })
})
