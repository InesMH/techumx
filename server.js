{}console.log(" IMH Funcionando con Nodemon");
var movimientosJSON=require('./movimientosv2.json')
var UsuariosJSON=require('./usuarios.json')
var express=require('express');
var bodyparser=require('body-parser')
var requestJson =require('request-json')

var app= express();
app.use(bodyparser.json())

app.get('/', function(req,res)
{
  res.send('Hola API')
})

app.get('/v1/movimientos', function(req,res)
{
  res.sendfile('movimientosv1.json')
})

app.get('/v2/movimientos', function(req,res)
{
  res.send(movimientosJSON)
})

app.get('/v2/movimientos/:id', function(req,res)
{
  //console.log(req)
  console.log(req.params.id)
  res.send(movimientosJSON[req.params.id-1])
})

app.post('/v2/movimientos', function(req,res){
  //console.log(req)
  //console.log(req.headers['authorization'])
//  if(req.headers['authorization']!=undefined)
  //{
    var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")
  //}
  //else{
   //res.send('No esta autorizado')
   //}
})

app.put('/v2/movimientos/:id', function(req,res)
  {
  var cambios = req.body
  var actual = movimientosJSON[req.params.id-1]
  if (cambios.Importe !=undefined)
  {
    actual.Importe = cambios.Importe
  }

  if (cambios.Ciudad !=undefined)
  {
    actual.Ciudad = cambios.Ciudad
  }

  res.send("Se ha modificado")
})

app.delete('/v2/movimientos/:id', function(req,res)
{
  var actual = movimientosJSON[req.params.id-1]
  movimientosJSON.push({
 "id":movimientosJSON.length+1,
 "Ciudad":actual.Ciudad,
  "Importe":actual.Importe*(-1),
  "Concepto":"Negativo del"+  req.params.id
  })
  res.send("Movimiento anulado")
})

app.get('/v2/movimientosq',function(req,res){
  console.log(req.query);
  res.send("recibido")
})

app.get('/v2/movimientosp/:id/:nombre',function(req,res){
  console.log(req.params)
  res.send("recibido")
})

app.get('/v2/usuarios/:id', function(req,res)
{
  //console.log(req)
  console.log(req.params.id)
  res.send(UsuariosJSON[req.params.id-1])
})

app.post('/v2/usuarios/login', function(req,res){
  var email = req.headers['email']
  var password = req.headers['password']
  var coinciden=false;
  for(var i=0;i<UsuariosJSON.length;i++){
    if(UsuariosJSON[i].email==email &&
       UsuariosJSON[i].password==password){
        coinciden=true;
    }
  }

  if(coinciden){
    res.send("Acceso correcto")
  }
  else{
    res.send("Acceso denegado")
  }
})

app.post('/v2/usuarios/logout/:id', function(req,res){
  var id = req.params.id
  var usuario= UsuariosJSON[id-1]
  if (usuario.estado==true){
    usuario.estado= false
    res.send('{"logout":"ok"}')
  }
  else
  {
    res.send('{"logout":"error"}')
  }
})


//Versiòn 3 de la API conectada a Mlab

var urlMLabRaiz="https://api.mlab.com/api/1/databases/techumxim/collections"
var apiKey= "apiKey=VEDuTy_vgd_6yEeD4VLZwcvDSQt0BU6N"
var clienteMlab = requestJson.createClient(urlMLabRaiz + "?" + apiKey)

//https://api.mlab.com/api/1/databases/techumxim/collections/?apiKey=VEDuTy_vgd_6yEeD4VLZwcvDSQt0BU6N
app.get('/v3', function(req,res){
  clienteMlab.get('', function(err,resM,body){
    var coleccionesUsuario=[]
    if (!err) {
      for (var i = 0; i < body.length; i++) {
        if (body[i]!="systems.indexes"){
  coleccionesUsuario.push({"recurso":body[i], "url":"/v3/" + body[i]})
   }
}
      res.send(coleccionesUsuario)
    }
    else{
    res.send(err)
  }
  })
})

app.get('/v3/usuarios', function(req,res) {
clienteMlab = requestJson.createClient(urlMLabRaiz + "/usuarios?" + apiKey)
clienteMlab.get('', function(err,resM,body){
  res.send(body)
})
})

app.get('/v3/usuarios/:id', function(req,res){
clienteMlab = requestJson.createClient(urlMLabRaiz + "/usuarios")
  clienteMlab.get('?q={"id":'+ req.params.id + '}&' + apiKey,
function(err,resM,body){
  res.send(body)
})
})

app.post('/v3/usuarios', function(req,res){
clienteMlab = requestJson.createClient(urlMLabRaiz + "/usuarios?" + apiKey)
clienteMlab.post('', req.body, function(err,resM, body){
  res.send(body)
})
})

/*app.put('/v3/usuarios', function(req,res){
clienteMlab = requestJson.createClient(urlMLabRaiz + "/usuarios?q={'usuarios'")
var cambio = '?q={"id":' + JSON.stringify(req.body + '}' )
clienteMlab.put('', JSON.parse(cambio), function(err,resM, body) {
  res.send(body)
})
})
*/

app.put('/v3/usuarios/:id',function(req,res){
  var query= '?q={"id":'+req.params.id+'}&'
  clienteMlab=requestJson.createClient(urlMLabRaiz + '/usuarios'+ query)
  var cambios = '{"$set":'+JSON.stringify(req.body)  +'}'
  clienteMlab.put(query+apiKey,JSON.parse(cambios),function(err, resM, body){
    res.send("Registro Modificado")
  })
})


app.get('/v3/movimientos', function(req,res) {
clienteMlab = requestJson.createClient(urlMLabRaiz + "/movimientos?" + apiKey)
clienteMlab.get('', function(err,resM,body){
  res.send(body)
})
})


app.get('/v3/movimientos/:id', function(req,res){
clienteMlab = requestJson.createClient(urlMLabRaiz + "/movimientos")
  clienteMlab.get('?q={"idcuenta":'+ req.params.id + '}&' + apiKey,
function(err,resM,body){
  res.send(body)
})
})

app.listen(3000);
console.log("Escuchando en el puerto 3000");
